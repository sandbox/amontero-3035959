<?php

/**
 * @file
 * Drush commands for database_connection_cache.
 */

/**
 * Implementation of hook_drush_command().
 */
function database_connection_cache_drush_command() {
  $items = array();

  $items['database-cache-bin-create'] = array(
    'description' => dt('Create the database table for a specified cache bin.'),
    'required-arguments' => TRUE,
    'arguments' => array(
      'bin' => dt('Cache bin name.'),
    ),
    'options' => array(
      'database' => array(
        'description' => dt('Optional. Database connection key. Default is NULL.'),
        'example-value' => 'key',
        'value' => 'required',
      ),
      'target' => array(
        'description' => dt('Optional. Database connection target. Default is \'default\'.'),
        'example-value' => 'target',
        'value' => 'required',
      ),
      'module' => array(
        'description' => dt('Optional. Module from which to get the table schema. Default is \'system\'.'),
        'example-value' => 'module',
        'value' => 'required',
      ),
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush database-cache-bin-create cache' => 'Creates the Drupal\'s default generic cache bin (\'cache\').',
      'drush database-cache-bin-create cache --target=cache' => 'Creates the Drupal\'s default generic cache bin (\'cache\') in the \'cache\' connection target.',
    ),
  );

  return $items;
}

function drush_database_connection_cache_database_cache_bin_create($bin) {
  $key = drush_get_option('database', NULL);
  $target = drush_get_option('target', 'default');
  $module = drush_get_option('module', 'system');

  $dt_args = array(
    '@bin'    => $bin,
    '@key'    => (is_null($key) ? 'NULL' : $key),
    '@target' => $target,
    '@module' => $module,
  );
  $msg_tail = ' on database \'@key\', target \'@target\'.';

  $msg = 'Cache bin table \'@bin\' creation requested' . $msg_tail;
  drush_log(dt($msg, $dt_args));

  $schema = drupal_get_schema_unprocessed($module);
  if (!isset($schema[$bin])) {
    $msg = 'No schema definition for the requested cache bin found in \'@module\' module.';
    $msg .= ' Using generic \'system\' module \'cache\' bin schema.';
    drush_log(dt($msg, $dt_args));
    $schema = $schema['cache'];
  } else {
    $schema = $schema[$bin];
  }

  Database::getConnection($target, $key)->schema()->createTable($bin, $schema);
  $msg = 'Cache bin table \'@bin\' created successfully' . $msg_tail;
  drush_log(dt($msg, $dt_args), 'ok');
}
